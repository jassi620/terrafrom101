
# normal EC2 instance
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance

provider "aws" {
  region            = "ap-southeast-1"
  access_key        = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
  secret_key        = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
}

terraform {
  required_version  = "~> 0.12.0"
}


resource "aws_instance" "one" {
  ami               = "ami-0f511ead81ccde020"
  instance_type     = "t2.micro"
  key_name          = "tf101"
  tags = {
    Name            = "amz_linux2_one"
  }
}


output "public_ip" {
  name = aws_instance.one.public_ip
}
