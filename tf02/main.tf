
provider "aws" {
  region            = var.region
  access_key        = var.aws_access_key
  secret_key        = var.aws_secret_key
}

terraform {
  required_version  = "~> 0.12.0"
}

# using default vpc
data "aws_vpc" "two" {
  default = true
}

data "aws_subnet_ids" "two" {
  vpc_id = data.aws_vpc.two.id
}

