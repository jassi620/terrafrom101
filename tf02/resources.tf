
# create a security group to ssh (22) & access http webserver ui (80)

resource "aws_security_group" "two" {
  name          = "sg for testing"
  description   = "Allow access to ssh & outbound all"
  vpc_id        = data.aws_vpc.two.id
  # incoming traffic
  ingress   = [
    {
      description        = "ssh to system"
      from_port          = 22
      to_port            = 22
      protocol           = "tcp"
      cidr_blocks        = ["0.0.0.0/0"]
      ipv6_cidr_blocks   = null
      prefix_list_ids    = null
      security_groups    = null
      self               = null
    },
    {
      description        = "web ui"
      from_port          = 80
      to_port            = 80
      protocol           = "tcp"
      cidr_blocks        = ["0.0.0.0/0"]
      ipv6_cidr_blocks   = null
      prefix_list_ids    = null
      security_groups    = null
      self               = null
    }
  ]
  # outgoing traffic
  egress   = [
    {
      description        = "all open"
      from_port          = 0
      to_port            = 0
      protocol           = "-1"
      cidr_blocks        = ["0.0.0.0/0"]
      ipv6_cidr_blocks   = null
      prefix_list_ids    = null
      security_groups    = null
      self               = null
    }
  ]

  tags                   = var.tags
}

data "template_file" "two" {
  template   = "${file("files/init.sh")}" 
}

# create ec2 instance + run user_data

resource "aws_instance" "two" {
  count                     = var.size
  ami                       = var.amazonLinux2
  instance_type             = var.amazonLinux2_instanceType
  subnet_id                 = element(tolist(data.aws_subnet_ids.two.ids), 0)
  vpc_security_group_ids    = [aws_security_group.two.id]
  key_name                  = "tf101"
  user_data                 = data.template_file.two.rendered

  root_block_device {
    volume_type             = "gp2"
    volume_size             = "8"
    delete_on_termination   = true
  }

  tags                      = var.tags

}


output "url" {
  value = "http://${aws_instance.two[0].public_ip}"
}
