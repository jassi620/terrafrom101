resource "aws_s3_bucket" "three" {
  bucket          = var.bucket_name
  acl             = var.acl
  force_destroy   = var.force_destroy

  versioning {
    enabled       = var.versioning
  }

  tags = {
    Name          = var.bucket_name
    Environment   = "Dev"
    Created_by    = "Terrform"
  }
}
