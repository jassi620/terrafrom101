variable "bucket_name" {
    default = "XXXXXXX"
}

variable "acl" {
    default = "private"
}

variable "versioning" {
    default = false
}

variable "force_destroy" {
    default = false
}
